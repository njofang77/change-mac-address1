
Ce script Python vous permet de changer l'adresse MAC d'une interface réseau sur un système Linux. Il utilise les outils système `ifconfig` pour désactiver temporairement l'interface, changer l'adresse MAC, puis réactiver l'interface avec la nouvelle adresse MAC.

## Utilisation

1. Assurez-vous que vous exécutez ce script en tant qu'administrateur (root) pour pouvoir modifier les paramètres de l'interface réseau.

2. Exécutez le script en utilisant la ligne de commande. Voici comment vous pouvez l'utiliser :

python3 mac_changer.py -i [interface] -m [nouvelle adresse MAC]


Remplacez `[interface]` par le nom de l'interface réseau que vous souhaitez modifier (par exemple, eth0) et `[nouvelle adresse MAC]` par la nouvelle adresse MAC que vous souhaitez attribuer à l'interface.

3. Le script affichera l'adresse MAC actuelle de l'interface sélectionnée, puis la changera pour la nouvelle adresse MAC spécifiée.

4. Si le changement d'adresse MAC est réussi, le script affichera un message indiquant que l'opération s'est déroulée avec succès. Sinon, il affichera un message d'erreur.

## Exemple d'utilisation

```shell
python3 mac_changer.py -i eth0 -m 00:11:22:33:44:55


Avertissement


L'utilisation incorrecte de ce script peut entraîner des problèmes de connectivité réseau. Assurez-vous de connaître les conséquences avant de changer l'adresse MAC de votre interface réseau.
