import re
import subprocess
import optparse

def search_mac_address(string):
    return re.search(r'(?:[0-9a-fA-F]:?){12}',str(string)).group(0)

def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i','--interface',dest='interface', help='interface to change his mac address')
    parser.add_option('-m','--mac',dest='new_mac_address', help='new mac address')
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error('[-]specify an interface use --help for more info')
    elif not options.new_mac_address:
        parser.error('[-] Please specify a new address --help for more info ')
    return options

def set_new_mac_address(interface, new_mac_address):
    print('change mac address for'+ interface +' to '+ new_mac_address)
    subprocess.call('ifconfig '+ interface +' down ', shell=True)
    subprocess.call('ifconfig '+ interface +' hw ether 00:11:22:33:44:55', shell=True)
    subprocess.call('ifconfig '+ interface +' up', shell=True)

def get_mac_address_from_interface(interface):
    ifconfig_result = subprocess.check_output(['ifconfig', interface])
    mac_address_search_result = search_mac_address(ifconfig_result)

    if mac_address_search_result:
        return  mac_address_search_result
    else:
        print('[-] could not read mac address.')

options = get_arguments()
current_mac_address = get_mac_address_from_interface(options.interface)
print('current Mac address for interface '+options.interface+' is : '+ current_mac_address)

set_new_mac_address(options.interface, options.new_mac_address)

new_mac_address = get_mac_address_from_interface(options.interface)

if new_mac_address == options.new_mac_address:
    print('[+] suuccessfuly : '+ new_mac_address )

else:
    print('[+] error mac address did not changed.')
